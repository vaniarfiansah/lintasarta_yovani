-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 30 Jul 2018 pada 13.20
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `la`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jar`
--

CREATE TABLE `jar` (
  `iddata` int(11) NOT NULL,
  `nojaringan` int(11) NOT NULL,
  `lokasi` varchar(50) NOT NULL,
  `jasa` varchar(50) NOT NULL,
  `bandwith` varchar(50) NOT NULL,
  `mediaakses` varchar(50) NOT NULL,
  `ipgateway` varchar(50) NOT NULL,
  `ipwanla` varchar(50) NOT NULL,
  `ipwan` varchar(50) NOT NULL,
  `iplan` varchar(50) NOT NULL,
  `subnetmask` varchar(50) NOT NULL,
  `ipatm` varchar(50) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `ket` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jar`
--

INSERT INTO `jar` (`iddata`, `nojaringan`, `lokasi`, `jasa`, `bandwith`, `mediaakses`, `ipgateway`, `ipwanla`, `ipwan`, `iplan`, `subnetmask`, `ipatm`, `kategori`, `ket`) VALUES
(28, 0, 'ui', 'bgh', 'u', 'rtg', 'u', 't', 'u', 'u', 'u', 'ui', 'u', 'u'),
(33, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(35, 3456789, 'dfghjhj', 'dcfvgbhn', 'dfghj', 'fgbhnjmk', '34567889', '45t6yu7i', '456789', '3456789', '255.255.255.248', '456789', 'sedfghjkhj', 'sdgfhjkljhn'),
(37, 2, 'rdfgu', 'drftgyuh', 'ftghu', 'guh', 'rtu', 't', 'rtu', 'rtuy', 'tu', 'rtuyi', 'ryth', 'dfguhijk');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `namauser` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`iduser`, `namauser`, `username`, `password`) VALUES
(2, 'vaniarfiansah', 'vani', 'vani'),
(3, 'khoirul', 'irul', 'irul');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jar`
--
ALTER TABLE `jar`
  ADD PRIMARY KEY (`iddata`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jar`
--
ALTER TABLE `jar`
  MODIFY `iddata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
