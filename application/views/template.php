<!doctype html>
<html lang="en">

<head>
	<title>LINTASARTA</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?=base_url();?>asset/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url();?>asset/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=base_url();?>asset/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<?=base_url();?>asset/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?=base_url();?>asset/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?=base_url();?>asset/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?=base_url();?>asset/img/ok.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?=base_url();?>asset/img/ok.png">
	<!-- DATA TABLE -->
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dataTable/datatables.min.css">
	<script type="text/javascript" src="<?=base_url();?>asset/vendor/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>asset/dataTable/datatables.min.js"></script>
	<!-- DATATABLE END -->

</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a><img style="width:61%" src="<?=base_url();?>asset/img/la.png" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?=base_url();?>asset/img/profil.png" class="img-circle" alt="Avatar"> <span>Account</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">
										<i class="lnr lnr-user"></i> 
										<span class="username"><?= $this->session->userdata('username');?></span>
									</a>
								</li>

								<li>
								<a href="<?php echo base_url(); ?>index.php/admin/logout">
									<i class="lnr lnr-exit"></i>Logout
								</a>

								</li>

							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<style>
div.hita
{
	font-text:arial;font-size: 2px;
	background-color:black;color:white;width:50px; text-align:center;
}
div.hitb
{
	background-color:blue;color:white;width:50px;
}
</style>
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar" style="margin-top: 3% ">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="<?=base_url();?>index.php/admin" class=""><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						<?php if ($this->session->userdata('level')=="admin"): ?>
						<li><a href="<?=base_url();?>index.php/jar" class=""><i class="lnr lnr-pencil"></i> <span>Input Data</span></a></li>
						<li><a href="<?=base_url();?>index.php/jar/detail_view" class=""><i class="lnr lnr-chart-bars"></i> <span>Data & Edit</span></a></li>
						<li><a href="<?=base_url();?>index.php/petugas" class=""><i class="lnr lnr-user"></i> <span>User Account</span></a></li>
						<li><a href="<?=base_url();?>index.php/jar/print_view" class=""><i class="lnr lnr-map"></i> <span>Print</span></a></li>

						<div style="margin-top: 75%; margin-left: 7%">
							<li>Copyright © 2018-2019</li>
							<li>PT. Aplikanusa Lintasarta.</li>
							<li>All rights reserved</li>
						</div>
						<?php endif ?>
						<?php if ($this->session->userdata('level')=="client"): ?>
						<li><a href="<?=base_url();?>index.php/jar/client_view"><i class="fa fa-bar-chart"></i> <span>Data</span></a></li>
						<div style="margin-top: 147%; margin-left: 11%">
							<li>Copyright © 2018-2019</li>
							<li>PT. Aplikanusa Lintasarta.</li>
							<li>All rights reserved</li>
						</div>
						<?php endif ?>
					</ul>
				</nav>
			</div>
		</div>
		
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<?php
                $this->load->view($main_view);
                ?>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			
		</footer>
	</div>
	<!-- END WRAPPER -->

	<!-- Javascript -->
	<script src="<?=base_url();?>asset/vendor/jquery/jquery.min.js"></script>
	<script src="<?=base_url();?>asset/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?=base_url();?>asset/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?=base_url();?>asset/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="<?=base_url();?>asset/vendor/chartist/js/chartist.min.js"></script>
	<script src="<?=base_url();?>asset/scripts/klorofil-common.js"></script>
	<!-- DATATABLE -->
	<script src="<?=base_url();?>asset/dataTable/datatables.js"></script>
	<!-- DATATABLE END -->

	<link rel="stylesheet" type="text/css" href="">
	<script type="text/javascript"></script>
	<script type="text/javascript"></script>
	<script type="text/javascript"></script>
	<script type="text/javascript"></script>

	<!-- export -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	<!-- export end -->

	<script>
	$(function() {
		var data, options;

		// headline charts
		data = {
			labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
			series: [
				[23, 29, 24, 40, 25, 24, 35],
				[14, 25, 18, 34, 29, 38, 44],
			]
		};

		options = {
			height: 300,
			showArea: true,
			showLine: false,
			showPoint: false,
			fullWidth: true,
			axisX: {
				showGrid: false
			},
			lineSmooth: false,
		};

		new Chartist.Line('#headline-chart', data, options);


		// visits trend charts
		data = {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			series: [{
				name: 'series-real',
				data: [200, 380, 350, 320, 410, 450, 570, 400, 555, 620, 750, 900],
			}, {
				name: 'series-projection',
				data: [240, 350, 360, 380, 400, 450, 480, 523, 555, 600, 700, 800],
			}]
		};

		options = {
			fullWidth: true,
			lineSmooth: false,
			height: "270px",
			low: 0,
			high: 'auto',
			series: {
				'series-projection': {
					showArea: true,
					showPoint: false,
					showLine: false
				},
			},
			axisX: {
				showGrid: false,

			},
			axisY: {
				showGrid: false,
				onlyInteger: true,
				offset: 0,
			},
			chartPadding: {
				left: 20,
				right: 20
			}
		};

		new Chartist.Line('#visits-trends-chart', data, options);


		// visits chart
		data = {
			labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
			series: [
				[6384, 6342, 5437, 2764, 3958, 5068, 7654]
			]
		};

		options = {
			height: 300,
			axisX: {
				showGrid: false
			},
		};

		new Chartist.Bar('#visits-chart', data, options);


		// real-time pie chart
		var sysLoad = $('#system-load').easyPieChart({
			size: 130,
			barColor: function(percent) {
				return "rgb(" + Math.round(200 * percent / 100) + ", " + Math.round(200 * (1.1 - percent / 100)) + ", 0)";
			},
			trackColor: 'rgba(245, 245, 245, 0.8)',
			scaleColor: false,
			lineWidth: 5,
			lineCap: "square",
			animate: 800
		});

		var updateInterval = 3000; // in milliseconds

		setInterval(function() {
			var randomVal;
			randomVal = getRandomInt(0, 100);

			sysLoad.data('easyPieChart').update(randomVal);
			sysLoad.find('.percent').text(randomVal);
		}, updateInterval);

		function getRandomInt(min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}

	});
	</script>
</body>

</html>
