<section class="panel panel-default" style="margin-top: 2%">
                    <header class="panel-heading">
                      HASIL DATA
                    </header>
                   <!--  UNTUK PRINT -->
                  
                    <!-- PRINT END -->
                    <div class="panel-body">
                        <div class="table-responsive">
                      <form class="form-horizontal bucket-form" method="get">
                              <table class="table table-bordered" id="example"> 
                                    <thead>
                                      <tr>
                                        <th>No. Jaringan</th>
                                        <th>Lokasi</th>
                                        <th>Jasa</th>
                                        <th>Bandwith</th>
                                        <th>Media akses</th>
                                        <th>IP Gateway</th>
                                        <th>IP WAN LA</th>
                                        <th>IP WAN</th>
                                        <th>IP LAN</th>
                                        <th>Subnetmask</th>
                                        <th>Kategori</th>
                                        <th>Pic</th>
                                        <th>Keterangan</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                               <?php
                                                        $no = 1;
                                                        foreach ($jar as $data) {
                                                            echo '
                                    <tr class="odd gradeX">
                                    <td>'.$data->nojaringan.'</td>
                                    <td>'.$data->lokasi.'</td>
                                    <td>'.$data->jasa.'</td>
                                    <td>'.$data->bandwith.'</td>
                                    <td>'.$data->mediaakses.'</td>
                                    <td>'.$data->ipgateway.'</td>
                                    <td>'.$data->ipwanla.'</td>
                                    <td>'.$data->ipwan.'</td>
                                    <td>'.$data->iplan.'</td>
                                    <td>'.$data->subnetmask.'</td>
                                    <td>'.$data->kategori.'</td>
                                    <td>'.$data->pic.'</td>
                                    <td>'.$data->ket.'</td>
                                    </tr>'
                                    ;
                                }

                                ?>
                                </tbody>
                            </table>
                    </form>
                    </div>
                </div>
            </section>

            <script type="text/javascript">
                $(document).ready
                (
                    function()
                {
                    var printCounter = 0;
                    $('#example').DataTable
                    (
                        {
                        dom: 'Bfrtip',
                        buttons: 
                        [
                            {
                                extend : 'excel',
                                title : 'LINTASARTA',
                            },
                            {
                                extend : 'pdf',
                                title : 'LINTASARTA',
                            },
                            {
                                extend : 'print',
                                title : 'LINTASARTA',
                            }
                        ]
                        }
                    );
                }
                );
            </script>
