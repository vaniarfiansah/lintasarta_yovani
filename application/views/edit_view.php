  <!--   <form class="form-horizontal bucket-form" method="post"  action="<?php echo base_url(); ?>index.php/jar/simpan" id="form-data" enctype="multipart/form-data"> -->
    <section class="panel panel-default" style="margin-top: 2%">
                    <header class="panel-heading">
                      HASIL DATA
                    </header>

                    <div class="panel-body">
                    <div class="table-responsive">
                      <form class="form-horizontal bucket-form" method="get">
                       
                              <table class="table table-bordered" id="example"> 
                                    <thead class="thead-light">
                                      <tr>
                                        <th>No. Jaringan</th>
                                        <th>Lokasi</th>
                                        <th>Jasa</th>
                                        <th>Bandwith</th>
                                        <th>Media akses</th>
                                        <th>IP Gateway</th>
                                        <th>IP WAN LA</th>
                                        <th>IP WAN</th>
                                        <th>IP LAN</th>
                                        <th>Subnetmask</th>
                                        <th>Kategori</th>
                                        <th>PIC</th>
                                        <th>Keterangan</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                               <?php
                                                        $no = 1;
                                                        foreach ($jar as $data) {
                                                            echo '
                                    <tr class="odd gradeX">
                                    <td>'.$data->nojaringan.'</td>
                                    <td>'.$data->lokasi.'</td>
                                    <td>'.$data->jasa.'</td>
                                    <td>'.$data->bandwith.'</td>
                                    <td>'.$data->mediaakses.'</td>
                                    <td>'.$data->ipgateway.'</td>
                                    <td>'.$data->ipwanla.'</td>
                                    <td>'.$data->ipwan.'</td>
                                    <td>'.$data->iplan.'</td>
                                    <td>'.$data->subnetmask.'</td>
                                    <td>'.$data->kategori.'</td>
                                    <td>'.$data->pic.'</td>
                                    <td>'.$data->ket.'</td>
                                    <td>
                                    
                                    <a href="#" class="btn btn-success glyphicon glyphicon-edit" data-toggle="modal" data-target="#modal_ubah'.$data->iddata.'">
                                    </a>
                                    <a href="'.base_url().'index.php/jar/hapus/'.$data->iddata.'" type="button" class="btn btn-danger glyphicon glyphicon-trash">
                                    </a>
                                    </td>
                                    </tr>'
                                    ;
                                }

                                ?>
                                </tbody>
                            </table>
                            </div>
                    </form>
                </div>
                </div>
            </section>
            

                               
    <!-- Modal Update -->
<?php 
    foreach ($jar as $id) 
    {
        echo '
             <div id="modal_ubah'.$id->iddata.'" tabindex="-1" role="dialog" aria-labelledby="modal_ubahLabel" class="modal fade" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title" id="modal_ubahLabel" style="padding-bottom: 20px; text-align: center;"><b>UBAH DATA</b></h4>
                </div>
                <form class="form-horizontal" action="'.base_url().'index.php/jar/edit/'.$id->iddata.'" method="post" enctype="multipart/form-data" role="form">
                    <div class="modal-body" style="text-align:left;">
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">No. Jaringan</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->nojaringan.'" class="form-control" name="nojaringan" id="nojaringan" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">Jasa</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->jasa.'" class="form-control" name="jasa" id="jasa" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">Media Akses</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->mediaakses.'" class="form-control" name="mediaakses" id="mediaakses" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">IP WAN LA</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->ipwanla.'" class="form-control" name="ipwanla" id="ipwanla">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">IP LAN</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->iplan.'" class="form-control" name="iplan" id="iplan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">Lokasi</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->lokasi.'" class="form-control" name="lokasi" id="lokasi" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">Bandwidth</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->bandwith.'" class="form-control" name="bandwith" id="bandwith">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">IP Gateway</label>
                            <div class="col-lg-9">
                                <input type="text"  value="'.$id->ipgateway.'" class="form-control" name="ipgateway" id="ipgateway">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">IP WAN</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->ipwan.'" class="form-control" name="ipwan" id="ipwan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">Subnetmask</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->subnetmask.'" class="form-control" name="subnetmask" id="subnetmask" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">Kategori</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->kategori.'" class="form-control" name="kategori" id="kategori" required>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">PIC</label>
                            <div class="col-lg-9">
                                <input type="text" value="'.$id->pic.'" class="form-control" name="pic" id="pic">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 col-sm-3 control-label">Keterangan</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" value="'.$id->ket.'" name="ket" id="ket"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" type="submit"> Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
        ';
    }
?>
<!-- </form> -->

        <script type="text/javascript">
                $(document).ready(function(){
                    $('#example').DataTable();
                });
        </script>