
        <section  style="margin-top:2% " class="panel panel-default">
            <header class="panel-heading">
                ADD DATA
            </header>
    <?php
        $notif = $this->session->flashdata('notif1');

        if(!empty($notif)){
            echo '<div class="alert alert-success">';
            echo $notif;
            echo '</div>';
        }
    ?>
    <?php
        $notif = $this->session->flashdata('notif_gagal');

        if(!empty($notif)){
            echo '<div class="alert alert-danger">';
            echo $notif;
            echo '</div>';
        }
    ?>
            <div class="panel-body">
            <form class="form-horizontal bucket-form" method="post"  action="<?php echo base_url(); ?>index.php/jar/simpan" id="form-data" enctype="multipart/form-data">

                    <div class="col-md-6">
                        <label for="nojaringan">No. Jaringan</label>
                        <input name="nojaringan" type="text" class="form-control" required>
                    </div>
                    <div class="col-md-6">
                        <label for="lokasi">Lokasi</label>
                        <input name="lokasi" type="text" class="form-control round-input" required>              
                      </div>
                    <div class="col-md-6">
                        <label for="jasa">Jasa</label>
                            <input name="jasa" type="text" class="form-control" required>
                    </div>
                    <div class="col-md-6">
                        <label for="bandwith">Bandwith</label>
                            <input  name="bandwith" type="text" class="form-control round-input">                        
                      </div>
                    <div class="col-md-6">
                        <label for="mediaakses">Media Akses</label>
                            <input name="mediaakses" type="text" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="ipgateway">IP Gateway</label>
                            <input name="ipgateway" type="text" class="form-control round-input">                        
                    </div>
                    <div class="col-md-6">
                        <label for="ipwanla">IP WAN LA</label>
                            <input name="ipwanla" type="text" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="ipwan">IP WAN</label>
                            <input name="ipwan" type="text" class="form-control round-input">                        
                    </div>
                    <div class="col-md-6">
                        <label for="iplan">IP LAN</label>
                            <input name="iplan" type="text" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="subnetmask">Subnetmask</label>
                            <input name="subnetmask" type="text" class="form-control round-input">                        
                    </div>
                    <div class="col-md-6">
                        <label for="kategori">Kategori</label>
                            <input name="kategori" type="text" class="form-control round-input" required>                        
                    </div>
                    <div class="col-md-6">
                        <label for="pic">PIC</label>
                            <input name="pic" type="text" class="form-control round-input">                        
                    </div>
                     <div class="col-md-12" style="padding-bottom: 17px;">
                        <label for="ket">Keterangan</label>
                            <input name="ket" type="text" class="form-control round-input">                        
                    </div>
                    <div class="col-md-6">
                        <input type="reset" name="reset" value="RESET" class="btn btn-block btn-md btn-danger">
                    </div>
                    <div class="col-md-6">
                        <input type="submit" name="submit" value="SIMPAN" class="btn btn-block btn-md btn-primary">
                    </div>
            </form>
            </div>
        </section>


              
   
    <!-- END Modal Update -->
    <!-- Javascript -->
<!--     <script type="text/javascript">
        function prepare_update_data(iddata)
        {
            $('#iddata').empty();
            $('#nojaringan').empty();
            $('#lokasi').empty();
            $('#jasa').empty();
            $('#bandwith').empty();
            $('#mediaakses').empty();
            $('#ipgateway').empty();
            $('#ipwanla').empty();
            $('#ipwan').empty();
            $('#iplan').empty();
            $('#subnetmask').empty();
            $('#ipatm').empty();
            $('#kategori').empty();
            $('#ket').empty();

            $.getJSON('<?php echo base_url(); ?>index.php/jar/get_data_by_id/' + iddata, function(data){
                $('#iddata').val(data.iddata);
                $('#nojaringan').val(data.nojaringan);
                $('#lokasi').val(data.lokasi);
                $('#jasa').val(data.jasa);
                $('#bandwith').val(data.bandwith);
                $('#mediaakses').val(data.mediaakses);
                $('#ipgateway').val(data.ipgateway);
                $('#ipwanla').val(data.ipwanla);
                $('#ipwan').val(data.ipwan);
                $('#iplan').val(data.iplan);
                $('#subnetmask').val(data.subnetmask);
                $('#ipatm').val(data.ipatm);
                $('#kategori').val(data.kategori);
                $('#ket').val(data.ket);
            });
        }
    </script> -->