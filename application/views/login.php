<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
  <title>LINTASARTA</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!-- VENDOR CSS -->
  <link rel="stylesheet" href="<?=base_url();?>asset/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url();?>asset/vendor/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=base_url();?>asset/vendor/linearicons/style.css">
  <!-- MAIN CSS -->
  <link rel="stylesheet" href="<?=base_url();?>asset/css/main.css">
  <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
  <link rel="stylesheet" href="<?=base_url();?>asset/css/demo.css">
  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  <!-- ICONS -->
  <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url();?>asset/img/ok.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?=base_url();?>asset/img/ok.png">
</head>

<body>
  <!-- WRAPPER -->
  <div id="wrapper">
    <div class="vertical-align-wrap">
      <div class="vertical-align-middle">
        <div class="auth-box ">
          <div class="left">
            <div class="content">
              <div class="header">
                <div class="logo text-center"><img src="<?=base_url();?>asset/img/la.png" ></div>
              </div>
                <?php
                        if(!empty($notif))
                        {
                            echo '<div class="alert alert-danger">';
                            echo $notif;
                            echo '</div>';
                        }
                    ?>
              <form method="post" action="<?php echo base_url(); ?>index.php/admin/masuk" id="form-login">
                <div class="form-group">
                  <label for="username" class="control-label sr-only">Username</label>
                  <input type="text" class="form-control" id="username"  placeholder="Username" name="username">
                </div>
                <div class="form-group">
                  <label for="password" class="control-label sr-only">Password</label>
                  <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
              </form>
            </div>
          </div>
          <div class="right">
            <div class="overlay"></div>
            <div class="content text">
                <h1 class="heading">PT Aplikanusa Lintasarta</h1>
              <p>innovative information communications solutions</p>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- END WRAPPER -->
</body>

</html>
