				<div class="container-fluid" style="margin-top: 1%">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-body">
							<div class="row">
								<a href="<?=base_url();?>index.php/admin">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="lnr lnr-home"></i></span>
										<p>
											<span style="margin-bottom: 11%" class="number">Dashboard</span>
										</p>
									</div>
								</div>
								</a>
								  <?php if ($this->session->userdata('level')=="admin"): ?>
								<a href="<?=base_url();?>index.php/jar">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="lnr lnr-pencil"></i></span>
										<p>
											<span style="margin-bottom: 11%" class="number">Input Data</span>
										</p>
									</div>
								</div>
								</a>
								<a href="<?=base_url();?>index.php/jar/detail_view">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="lnr lnr-chart-bars"></i></span>
										<p>
											<span style="margin-bottom: 11%" class="number">Data & Edit</span>
										</p>
									</div>
								</div>
								</a>
								<a href="<?=base_url();?>index.php/petugas">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="lnr lnr-user"></i></span>
										<p>
											<span style="margin-bottom: 11%" class="number">Account</span>
										</p>
									</div>
								</div>
								</a>
								<?php endif ?>
								<?php if ($this->session->userdata('level')=="client"): ?>
								<a href="<?=base_url();?>index.php/jar/client_view">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bar-chart"></i></span>
										<p>
											<span style="margin-bottom: 11%" class="number">Data</span>
										</p>
									</div>
								</div>
								</a>
								<?php endif ?>
							</div>
							<?php if ($this->session->userdata('level')=="admin"): ?>
							<div class="row">
								<div class="col-md-12">
									<a style="font-size: 255%;"><marquee direction="left" scrollamount="11" align="center">Selamat datang, <?= $this->session->userdata('username');?>.</marquee></a>
									<img style="width: 100%" src="<?=base_url();?>asset/img/arta.png">
								</div>
							</div>
							<?php endif ?>
							<?php if ($this->session->userdata('level')=="client"): ?>
							<div class="row">
								<div class="col-md-12">
									<a style="font-size: 255%;"><marquee direction="left" scrollamount="11" align="center">Selamat datang, <?= $this->session->userdata('username');?>.</marquee></a>
									<img style="width: 100%" src="<?=base_url();?>asset/img/hai.jpg">
								</div>
							</div>
							<?php endif ?>

						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>