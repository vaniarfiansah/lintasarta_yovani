
    <form class="form-horizontal bucket-form" method="post"  action="<?php echo base_url(); ?>index.php/petugas/simpan" id="form-petugas">

        <section  style="margin-top:2% " class="panel panel-default">
            <header class="panel-heading">
                ADD DATA
            </header>
    <?php
        $notif = $this->session->flashdata('notif1');

        if(!empty($notif)){
            echo '<div class="alert alert-success">';
            echo $notif;
            echo '</div>';
        }
    ?>
    <?php
        $notif = $this->session->flashdata('notif_gagal');

        if(!empty($notif)){
            echo '<div class="alert alert-danger">';
            echo $notif;
            echo '</div>';
        }
    ?>
            <div class="panel-body">
            <form class="form-horizontal bucket-form" method="post"  action="<?php echo base_url(); ?>index.php/petugas/simpan" id="form-petugas" enctype="multipart/form-data">

                    <div class="col-md-12">
                        <label for="namauser">Nama User</label>
                        <input name="namauser" type="text" class="form-control" required>
                    </div>
                    <div class="col-md-12">
                        <label for="username">Username</label>
                        <input name="username" type="text" class="form-control" required>              
                      </div>
                    <div class="col-md-12">
                        <label for="password">Password</label>
                            <input name="password" type="text" class="form-control" required>
                    </div>
                     <div class="col-md-12">
                        <label class="control-label">Level</label>
                        <select name="level" class="form-control" ng-model="model.select" required="">
                            <option value="? undefined:undefined ?"></option>
                            <option value="admin">Admin</option>
                            <option value="kasir">Client</option>
                        </select>
                    </div>
                    
                    <div style="margin-top: 2%" class="col-md-6">
                        <input type="reset" name="reset" value="RESET" class="btn btn-block btn-md btn-danger">
                    </div>
                    <div style="margin-top: 2%" class="col-md-6">
                        <input type="submit" name="submit" value="SIMPAN" class="btn btn-block btn-md btn-primary">
                    </div>
            </form>
            </div>
        </section>


               <!-- DATA PETUGAS START -->
      <section class="panel panel-default" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body">
         <header class="panel-heading">
            DATA PETUGAS
        </header>
         <?php
        $notif = $this->session->flashdata('notif');

        if(!empty($notif)){
        echo '<div class="alert alert-success">'.$notif.'</div>';
        }
    ?>
        
          <table class="table table-bordered"> 
            <thead>
              <tr>
                  <th>No</th>
                  <th>Nama Petugas</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Level</th>
                  <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
                                        $no = 1;
                                        foreach ($petugas as $data) {
                                            echo "
                                            <tr class='odd gradeX'>
                                            <td>".$no++."</td>
                                            <td>$data->namauser</td>
                                            <td>$data->username</td>
                                            <td>$data->password</td>
                                            <td>$data->level</td>
                                            <td><button class='btn btn-success glyphicon glyphicon-edit' data-toggle='modal' data-target='#modal$data->iduser'></button>
                                            <a href='".base_url()."index.php/petugas/hapus/$data->iduser' type='button' class='btn btn-danger glyphicon glyphicon-trash'></a></td>
                                            </tr>
                                            <!-- Modal -->
                                        <div class='modal fade' id='modal$data->iduser' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                            <div class='modal-dialog'>
                                                <div class='modal-content'>
                                                  <form role='form' action='".base_url()."index.php/petugas/edit/".$data->iduser."' method='post'>
                                                    <div class='modal-header'>
                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                        <h4 class='modal-title' id='myModalLabel'>Edit Data Petugas</h4>
                                                    </div>
                                                    <div class='modal-body'>";?>
           
            <div class="panel-body">
            <div class="col-md-12">
              <label for="namauser" class="control-label">Nama User</label>
              <input type="text" class="form-control" required name="namauser" value="<?php echo $data->namauser?>">
            </div>
            <div class="col-md-12">
              <label for="username" class="control-label">Username</label>
              <input type="text" class="form-control" required name="username" value="<?php echo $data->username?>">
            </div>
            <div class="col-md-12">
              <label for="password" class="control-label">Password</label>
              <input type="password" class="form-control" required name="password" value="<?php echo $data->password?>">
            </div>
            <div class="col-md-12">
              <label class="control-label">Level</label>
              <select name="level" class="form-control" required>
                <option value="admin">Admin</option>
                <option value="client" <?php if($data->level=='client'){echo 'selected';} ?>>client</option>
              </select>
            </div>
            </div>
          
           
                                                    <?php echo "</div>
                                                    <div class='modal-footer'>
                                                    <div class='row'>
                                                    <div class='col-lg-6'>
                                                    <input type='button' name='cancel' class='btn btn-danger btn-block' data-dismiss='modal' value='Cancel'>
                                                    </div>
                                                    <div class='col-lg-6'>
                                                    <input type='submit' name='submit' class='btn btn-success btn-block' value='Edit'>
                                                    </div>
                                                </div>

                                                    </form>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                      ";
                                    }
                                  ?>
            </tbody>
        </table>
    </div>
      </section>
    <!-- DATA PETUGAS FINISH -->