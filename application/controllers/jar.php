<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class jar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->load->model('jar_model');
	}

	public function detail_view()
	{
		$data['jar'] = $this->jar_model->get_data_jar();
		$data['main_view'] = 'edit_view';
		$this->load->view('template', $data);
	}

	public function print_view()
	{
		$data['jar'] = $this->jar_model->get_data_jar();
		$data['main_view'] = 'print';
		$this->load->view('template', $data);
	}

	public function client_view()
	{
		$data['jar'] = $this->jar_model->get_data_jar();
		$data['main_view'] = 'client';
		$this->load->view('template', $data);
	}

	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){
			$data['jar'] = $this->jar_model->get_data_jar();
			$data['main_view'] = 'jar_view';
			$this->load->view('template', $data);
		} else {
			redirect('admin');
		}	
	}

	public function simpan()
	{
		if($this->jar_model->simpan() == TRUE){
			$this->session->set_flashdata('notif1', 'Data Berhasil ditambah');
            redirect('jar');
		} else {
			$this->session->set_flashdata('notif_gagal', 'Data Gagal berhasil');
            redirect('jar');
		}
	}


	public function hapus($id_jar)
	{
		if($this->jar_model->delete($id_jar) == TRUE){
			$this->session->set_flashdata('notif_gagal', 'data Berhasil Dihapus');
			redirect('jar/detail_view');
		} else {
			$this->session->set_flashdata('notif1', 'data Gagal Dihapus');
			redirect('jar');
		}
	}

	public function get_data_by_id($iddata)
	{
		$get_data_by_id = $this->jar_model->get_data_by_id($iddata);
		echo json_encode($get_data_by_id);
	}

	public function edit($iddata)
	{
		if($this->jar_model->edit($iddata) == TRUE){
			$this->session->set_flashdata('notif1', 'Edit data berhasil');
			redirect('jar/detail_view');
		} else {
			$this->session->set_flashdata('notif_gagal', 'Edit data gagal');
            redirect('jar');
		}
	}

	public function detil($id)
	{
		$data['detil'] = $this->jar_model->get_detil_jar($id)->result_object();
	}

}
