<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class jar_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function simpan()
	{
		$data = array(
				'nojaringan'=>$this->input->post('nojaringan'),
				'lokasi'	=> $this->input->post('lokasi'),
				'jasa'		=> $this->input->post('jasa'),
				'bandwith'	=> $this->input->post('bandwith'),
				'mediaakses'=> $this->input->post('mediaakses'),
				'ipgateway'	=> $this->input->post('ipgateway'),
				'ipwanla'	=> $this->input->post('ipwanla'),
				'ipwan'		=> $this->input->post('ipwan'),
				'iplan'		=> $this->input->post('iplan'),
				'subnetmask'=> $this->input->post('subnetmask'),
				'kategori'	=> $this->input->post('kategori'),
				'pic'		=> $this->input->post('pic'),
				'ket'		=> $this->input->post('ket'),
			);

		$this->db->insert('jar',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get_data_by_id($iddata)
	{
		return $this->db->where('iddata', $iddata)
						->get('jar')
						->row();
	}

	public function edit($iddata)
	{
		$data = array(
				'nojaringan'	=> $this->input->post('nojaringan'),
				'lokasi'		=> $this->input->post('lokasi'),
				'jasa'			=> $this->input->post('jasa'),
				'bandwith'		=> $this->input->post('bandwith'),
				'mediaakses'	=> $this->input->post('mediaakses'),
				'ipgateway'		=> $this->input->post('ipgateway'),
				'ipwanla'		=> $this->input->post('ipwanla'),
				'ipwan'			=> $this->input->post('ipwan'),
				'iplan'			=> $this->input->post('iplan'),
				'subnetmask'	=> $this->input->post('subnetmask'),
				'kategori'		=> $this->input->post('kategori'),
				'pic'			=> $this->input->post('pic'),
				'ket'			=> $this->input->post('ket'),
			);

		$this->db->where('iddata', $iddata)
				->update('jar', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function get_detil_jar($id)
	{
		return $this->db->get_where('jar',array('iddata'=>$id));
	}

	public function get_data_jar()
	{
		return $this->db->order_by('iddata','ASC')
						->get('jar')
						->result();
	}

	public function total_records()
	{
		return $this->db->from('jar')
						->count_all_result();
	}

	public function delete($id_jar)
	{
		$this->db->where('iddata',$id_jar)
				 ->delete('jar');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
